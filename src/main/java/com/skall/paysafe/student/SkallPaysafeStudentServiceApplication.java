
package com.skall.paysafe.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan
@EnableJpaRepositories(
        basePackages = {
            "com.skall.paysafe.student.database.repository"
        })
@EntityScan(
        basePackages = {
            "com.skall.paysafe.student.database.entity"
        })
public class SkallPaysafeStudentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkallPaysafeStudentServiceApplication.class, args);
    }

}
