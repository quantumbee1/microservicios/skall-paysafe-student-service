
package com.skall.paysafe.student.rest.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AuthenticationResponse {
    private String token;
    private String sessionId;
    private Boolean changePassword;
    private String apiKey;
    private String apiLogin;
}
