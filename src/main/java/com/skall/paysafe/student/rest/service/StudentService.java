
package com.skall.paysafe.student.rest.service;

import com.skall.paysafe.student.rest.dto.DetailUserUpdateRequest;
import com.skall.paysafe.student.rest.dto.RegisterStudentResponse;
import com.skall.paysafe.student.rest.dto.StudentRegisterRequest;
import com.skall.paysafe.student.rest.model.GenericResponse;

public interface StudentService {
    public GenericResponse<RegisterStudentResponse> register(String serviceName, String ipAddress,
            StudentRegisterRequest request);

    public GenericResponse<Void> detailUpdate(String serviceName, String ipAddres,
            DetailUserUpdateRequest request);
}
