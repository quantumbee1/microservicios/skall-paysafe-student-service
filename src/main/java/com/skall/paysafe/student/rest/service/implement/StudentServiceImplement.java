
package com.skall.paysafe.student.rest.service.implement;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.skall.paysafe.student.constants.Constants;
import com.skall.paysafe.student.constants.ExceptionMessages;
import com.skall.paysafe.student.constants.RoleStatus;
import com.skall.paysafe.student.constants.StatusCode;
import com.skall.paysafe.student.database.entity.AcademicTrainingEntity;
import com.skall.paysafe.student.database.entity.AcademicTrainingUserEntity;
import com.skall.paysafe.student.database.entity.DocumentTypeEntity;
import com.skall.paysafe.student.database.entity.InterestEntity;
import com.skall.paysafe.student.database.entity.InterestUserEntity;
import com.skall.paysafe.student.database.entity.ParlorEntity;
import com.skall.paysafe.student.database.entity.RolEntity;
import com.skall.paysafe.student.database.entity.RolUserEntity;
import com.skall.paysafe.student.database.entity.SkillEntity;
import com.skall.paysafe.student.database.entity.SkillUserEntity;
import com.skall.paysafe.student.database.entity.UserEntity;
import com.skall.paysafe.student.database.repository.AcademicTrainingRepository;
import com.skall.paysafe.student.database.repository.AcademicTrainingUserRepository;
import com.skall.paysafe.student.database.repository.DocumentTypeRepository;
import com.skall.paysafe.student.database.repository.InterestRepository;
import com.skall.paysafe.student.database.repository.InterestUserRepository;
import com.skall.paysafe.student.database.repository.ParlorRepository;
import com.skall.paysafe.student.database.repository.RolRepository;
import com.skall.paysafe.student.database.repository.RolUserRepository;
import com.skall.paysafe.student.database.repository.SkillRepository;
import com.skall.paysafe.student.database.repository.SkillUserRepository;
import com.skall.paysafe.student.database.repository.UserRepository;
import com.skall.paysafe.student.handler.exception.DocumentTypeNotFoundException;
import com.skall.paysafe.student.handler.exception.RoleUserNotFoundException;
import com.skall.paysafe.student.handler.exception.UserEmailFoundException;
import com.skall.paysafe.student.rest.dto.DetailUserUpdateRequest;
import com.skall.paysafe.student.rest.dto.RegisterStudentResponse;
import com.skall.paysafe.student.rest.dto.StudentRegisterRequest;
import com.skall.paysafe.student.rest.model.GenericResponse;
import com.skall.paysafe.student.rest.service.StudentService;
import com.skall.util.RandomString;

@Service
public class StudentServiceImplement implements StudentService {
    private static final Logger logger = LoggerFactory.getLogger(StudentServiceImplement.class);
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RolRepository rolRepository;
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private SkillUserRepository skillUserRepository;
    @Autowired
    private InterestRepository interestRepository;
    @Autowired
    private AcademicTrainingRepository academicTrainingRepository;
    @Autowired
    private AcademicTrainingUserRepository academicTrainingUserRepository;
    @Autowired
    private InterestUserRepository interestUserRepository;
    @Autowired
    private RolUserRepository rolUserRepository;
    @Autowired
    private ParlorRepository parlorRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private DocumentTypeRepository documentTypeRepository;

    @Qualifier("skall-apikey")
    @Autowired
    private RandomString merchantRamdomApikey;
    @Qualifier("skall-apikey")
    @Autowired
    private RandomString merchantRamdomApiLogin;
    @Qualifier("skall-publickey")
    @Autowired
    private RandomString merchantRamdomPublicKey;

    @Override
    public GenericResponse<RegisterStudentResponse> register(String serviceName, String ipAddress,
            StudentRegisterRequest request) {

        logger.info("{} init -> service-name[{}] - request[{}] - ip-address[{}]", "RegisterUser",
                serviceName, request, ipAddress);

        Optional<DocumentTypeEntity> documentType =
                documentTypeRepository.findByName(request.getDocumentType());

        Optional<UserEntity> usuario =
                userRepository.findByEmailAndStatusIsTrue(request.getEmail());

        if (usuario.isPresent())
            throw new UserEmailFoundException(ExceptionMessages.USER_EMAIL_FOUND);
        if (!documentType.isPresent())
            throw new DocumentTypeNotFoundException(ExceptionMessages.DOCUMENT_TYPE_NOT_FOUND);

        DocumentTypeEntity document = documentType.get();

        final String apiKey = merchantRamdomApikey.nextString();
        final String apiLogin = merchantRamdomApiLogin.nextString();
        final String publicKey = merchantRamdomPublicKey.nextString();

        ParlorEntity parlor = new ParlorEntity();

        if (request.getPhoneNumber() == null)
            parlor.setPhoneNumber("00000000");

        parlor.setApiKey(apiKey);
        parlor.setApiLogin(apiLogin);
        parlor.setParlorDescription(Constants.PARLOR_DEFAULT_DESCRIPTION);
        parlor.setDocumentNumber(request.getDocumentNumber());
        parlor.setDocumentTypeId(document);
        parlor.setEmail(request.getEmail());
        parlor.setStatus(false);
        parlor.setLogo(null);
        parlor.setName(Constants.PARLOR_DEFAULT_NAME);
        parlor.setPublicKey(publicKey);
        parlor.setPhoneNumber(request.getPhoneNumber());
        parlor.setSubscribers(0);
        parlor.setNotifyUrl(Constants.PARLOR_NOTIFY_URL_DEFAULT);

        parlorRepository.save(parlor);

        UserEntity studentEntity = new UserEntity();
        studentEntity.setFirstName(request.getFirstName());
        studentEntity.setLastName(request.getLastName());
        studentEntity.setSecondLastName(request.getSecondLastName());
        studentEntity.setEmail(request.getEmail());
        studentEntity.setStatus(true);
        studentEntity.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        studentEntity.setDocumentTypeId(document);
        studentEntity.setDocumentNumber(request.getDocumentNumber());
        studentEntity.setPhoneNumber(request.getPhoneNumber());
        studentEntity.setParlorId(parlor);
        studentEntity.setVerify(false);
        userRepository.save(studentEntity);

        RolEntity rol = rolRepository.findByName(RoleStatus.GENERAL.getDescription());

        if (rol == null) {
            throw new RoleUserNotFoundException(
                    "Ha ocurrido un error, por favor persista mas tarde.");
        }
        RolUserEntity rolUser = new RolUserEntity();
        rolUser.setStatus(true);
        rolUser.setUserId(studentEntity);
        rolUser.setRolId(rol);
        rolUserRepository.save(rolUser);

        RegisterStudentResponse data = new RegisterStudentResponse();
        data.setEmail(request.getEmail());

        GenericResponse<RegisterStudentResponse> response = new GenericResponse<>();
        response.setData(data);
        response.setMessage(StatusCode.OK.getDescription());
        response.setStatusCode(StatusCode.OK.getCode());

        logger.info("{} OK -> service-name[{}] - response[{}] - ip-address[{}]", "RegisterUser",
                serviceName, response, ipAddress);

        return response;
    }

    @Override
    public GenericResponse<Void> detailUpdate(String serviceName, String ipAddres,
            DetailUserUpdateRequest request) {

        logger.info("{} init -> service-name[{}] - request-id[{}] - ip-address[{}]",
                "DetailUpdateUser", serviceName, request, ipAddres);
        Optional<UserEntity> usuario =
                userRepository.findByEmailAndStatusIsTrueAndVerifyIsFalse(request.getEmail());

        if (!usuario.isPresent()) {
            throw new UserEmailFoundException(ExceptionMessages.API_KEY_AND_API_LOGIN_INVALID);
        }

        UserEntity student = usuario.get();
        Optional<ParlorEntity> parlor = parlorRepository.findById(student.getParlorId().getId());

        if (!parlor.isPresent()) {
            throw new UserEmailFoundException(ExceptionMessages.PARLOR_NOT_FOUND);
        }
        ParlorEntity palorUpdate = parlor.get();

        palorUpdate.setName(request.getParlorName());
        palorUpdate.setParlorDescription(request.getParlorDescription());

        student.setAge(request.getAge());
        student.setGender(request.getGender());
        student.setVerify(true);
        userRepository.save(student);
        parlorRepository.save(palorUpdate);

        for (String Interest : request.getInterest()) {
            Optional<InterestEntity> interest = interestRepository.findByName(Interest);
            if (interest.isPresent()) {
                InterestEntity data = interest.get();
                InterestUserEntity interestUser = new InterestUserEntity();
                interestUser.setStatus(true);
                interestUser.setInterestId(data);
                interestUser.setUserId(student);
                interestUserRepository.save(interestUser);
            }
        }

        for (String Skill : request.getSkill()) {
            Optional<SkillEntity> skill = skillRepository.findByName(Skill);
            SkillEntity data = new SkillEntity();
            SkillUserEntity skillUser = new SkillUserEntity();

            if (!skill.isPresent()) {
                data.setName(Skill);
                data.setStatus(true);
                skillRepository.save(data);
            } else {
                data = skill.get();
            }
            skillUser.setUserId(student);
            skillUser.setStatus(true);
            skillUser.setSkillId(data);
            skillUserRepository.save(skillUser);

        }

        Optional<AcademicTrainingEntity> academic = academicTrainingRepository
                .findByNameAndSpecialty(request.getInstitution(), request.getProfesion());
        AcademicTrainingEntity data = new AcademicTrainingEntity();
        AcademicTrainingUserEntity academicUser = new AcademicTrainingUserEntity();

        if (!academic.isPresent()) {
            data.setName(request.getInstitution());
            data.setSpecialty(request.getProfesion());
            academicTrainingRepository.save(data);
        } else {
            data = academic.get();
        }
        academicUser.setEndTraining(request.getDateFinish());
        academicUser.setLevelOfStudy(request.getLevelOfStudy());
        academicUser.setStatus(true);
        academicUser.setAcademicTrainingId(data);
        academicUser.setUserId(student);
        academicUser.setStartTraining(request.getDateInit());

        academicTrainingUserRepository.save(academicUser);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(StatusCode.OK.getDescription());
        response.setStatusCode(StatusCode.OK.getCode());
        return response;
    }

}
