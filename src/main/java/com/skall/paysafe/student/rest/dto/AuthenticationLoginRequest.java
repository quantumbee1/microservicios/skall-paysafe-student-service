
package com.skall.paysafe.student.rest.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AuthenticationLoginRequest {
    @NotNull
    @NotEmpty
    @Email
    private String userName;

    @NotNull
    @NotEmpty
    private String password;
}
