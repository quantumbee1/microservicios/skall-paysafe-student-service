
package com.skall.paysafe.student.rest.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DetailUserUpdateRequest {
    @NotNull
    @NotEmpty
    private List<String> interest;
    @NotNull
    @NotEmpty
    private List<String> skill;
    @NotNull
    @NotEmpty
    private String institution;
    @NotNull
    @NotEmpty
    private String levelOfStudy;
    @NotNull
    @NotEmpty
    private String profesion;
    @NotNull
    @NotEmpty
    private String age;
    @NotNull
    @NotEmpty
    private String gender;
    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    @NotEmpty
    private String parlorName;
    @NotNull
    @NotEmpty
    private String parlorDescription;

    private Date dateInit;
    private Date dateFinish;
}
