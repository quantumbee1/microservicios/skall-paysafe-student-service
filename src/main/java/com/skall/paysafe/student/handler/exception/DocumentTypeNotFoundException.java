
package com.skall.paysafe.student.handler.exception;

public class DocumentTypeNotFoundException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 2079546358302350755L;

    public DocumentTypeNotFoundException(final String message) {
        super(message);
    }
}
