
package com.skall.paysafe.student.handler.exception;

public class RoleUserNotFoundException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -334198210281129494L;

    public RoleUserNotFoundException(final String message) {
        super(message);
    }
}
