
package com.skall.paysafe.student.handler.exception;

public class UserEmailFoundException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = -298979425194133461L;

    public UserEmailFoundException(final String message) {
        super(message);
    }
}
