
package com.skall.paysafe.student.config;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfiguration {
    @Value("${skall.paysafe.service.connect-timeout:60}")
    private Integer connectTimeout;
    @Value("${peiky.payment.service.read-timeout:30}")
    private Integer readTimeout;

    @LoadBalanced
    @Bean
    @Primary
    public RestTemplate getRestTemplate() {
        return createRestTemplate(connectTimeout, readTimeout);
    }

    private RestTemplate createRestTemplate(final int connectTimeout, final int readTimeout) {

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        return restTemplateBuilder.setConnectTimeout(Duration.ofSeconds(connectTimeout))
                .setReadTimeout(Duration.ofSeconds(readTimeout)).build();
    }
}
