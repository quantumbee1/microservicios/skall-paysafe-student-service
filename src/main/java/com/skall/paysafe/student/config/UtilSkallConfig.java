
package com.skall.paysafe.student.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.skall.util.RandomString;

@Configuration
@RefreshScope
public class UtilSkallConfig {

    @Value("${skall.paysafe.parlor.apikey.length}")
    private Integer parlorApikeyLength;

    @Value("${skall.paysafe.parlor.apikey.alphabet}")
    private String parlorApikeyAlphabet;

    @Value("${skall.paysafe.parlor.apilogin.length:50}")
    private Integer parlorApiloginLength;

    @Value("${skall.paysafe.parlor.apilogin.alphabet}")
    private String parlorApiloginAlphabet;

    @Value("${skall.paysafe.parlor.publickey.length}")
    private Integer parlorPublicKeyLength;

    @Value("${skall.paysafe.parlor.publickey.alphabet}")
    private String parlorPublicKeyAlphabet;

    @Bean("skall-apikey")
    public RandomString configRandomStringMerchantApiKey() {
        return new RandomString(parlorApikeyLength, parlorApikeyAlphabet);
    }

    @Bean("skall-apilogin")
    public RandomString configRandomStringMerchantApiLogin() {
        return new RandomString(parlorApiloginLength, parlorApiloginAlphabet);
    }

    @Bean("skall-publickey")
    public RandomString configRandomStringMerchantPublicKey() {
        return new RandomString(parlorPublicKeyLength, parlorPublicKeyAlphabet);
    }

}
