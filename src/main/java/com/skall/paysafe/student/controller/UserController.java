
package com.skall.paysafe.student.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.skall.paysafe.student.constants.Constants;
import com.skall.paysafe.student.rest.dto.DetailUserUpdateRequest;
import com.skall.paysafe.student.rest.model.GenericResponse;
import com.skall.paysafe.student.rest.service.StudentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/user")
@RestController
@Api(
        value = "/user",
        produces = "application/json")
@ApiResponses(
        value = {
            @ApiResponse(
                    code = 200,
                    message = "ok"),
            @ApiResponse(
                    code = 500,
                    message = "internal server error"),
            @ApiResponse(
                    code = 400,
                    message = "bad request"),
            @ApiResponse(
                    code = 405,
                    message = "method not allowed"),
            @ApiResponse(
                    code = 415,
                    message = "unsupported media type"),
            @ApiResponse(
                    code = 406,
                    message = "not acceptable"),
            @ApiResponse(
                    code = 404,
                    message = "not found"),
            @ApiResponse(
                    code = 503,
                    message = "service unavailable")
        })
@CrossOrigin(
        origins = {
            "http://localhost:4200"
        })
public class UserController {
    @Autowired
    private StudentService studentService;

    @RequestMapping(
            path = "/detailUpdate",
            method = RequestMethod.PUT)
    public ResponseEntity<GenericResponse<Void>> detailUpdate(@ApiParam(
            value = Constants.DESCRIPTION_SERVICE_NAME,
            defaultValue = Constants.SERVICE_NAME_DETAIL_USER_UPDATE) @RequestHeader(
                    name = Constants.HEADER_KEY_SERVICE_NAME,
                    required = false) final String serviceName,
            @ApiParam(
                    value = Constants.DESCRIPTION_IP_ADDRESS,
                    defaultValue = Constants.EXAMPLE_IP_ADDRESS) @RequestHeader(
                            name = Constants.HEADER_KEY_IP_ADDRESS,
                            required = false) final String ipAddres,
            @RequestBody DetailUserUpdateRequest request) {
        return ResponseEntity.ok().header(Constants.HEADER_KEY_SERVICE_NAME, serviceName)
                .body(studentService.detailUpdate(serviceName, ipAddres, request));
    }

}
