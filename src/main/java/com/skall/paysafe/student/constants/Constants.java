
package com.skall.paysafe.student.constants;

public class Constants {
    public static final String HEADER_KEY_SERVICE_NAME = "service-name";
    public static final String HEADER_KEY_IP_ADDRESS = "x-forwarded-for";
    public static final String DESCRIPTION_SERVICE_NAME =
            "Este header no es obligatorio, el valor enviado por el cliente sera retornado en un header del mismo nombre.";
    public static final String DESCRIPTION_IP_ADDRESS =
            "Este header es obligatorio, pero cuando se consume el servicio por medio del gateway este se ancarga del envio de la misma.";
    public static final String PARLOR_DEFAULT_NAME = "Salon";
    public static final String PARLOR_DEFAULT_DESCRIPTION = "Agregar una descripción";
    public static final String PARLOR_NOTIFY_URL_DEFAULT = "www.MiSalon.com";
    public static final String EXAMPLE_IP_ADDRESS = "172.0.0.1";
    // STUDENT
    public static final String SERVICE_NAME_LOGIN_CLIENTSTUDENT = "LoginAuthClientStudent";
    public static final String SERVICE_NAME_REGISTER_USER = "RegisterUser";
    public static final String SERVICE_NAME_DETAIL_USER_UPDATE = "DetailUserUpdate";
    public static final String HEADER_KEY_SESSION_ID = "sessionId";
    public static final String KEY_ROLES = "roles";

    public Constants() {
    }

}
