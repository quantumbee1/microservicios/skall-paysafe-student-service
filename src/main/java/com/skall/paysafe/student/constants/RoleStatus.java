
package com.skall.paysafe.student.constants;

import lombok.Getter;

@Getter
public enum RoleStatus {
    GENERAL("ROLE_USER"), ADMIN("ROLE_ADMIN");

    private String description;

    private RoleStatus(String description) {
        this.description = description;
    }
}
