
package com.skall.paysafe.student.constants;

import lombok.Getter;

@Getter
public class ExceptionMessages {
    public static final String DEFAULT =
            "Ha ocurrido un error inesperado, por favor intente mas tarde.";
    public static final String DOCUMENT_TYPE_NOT_FOUND = "Tipo de documento no encontrado";
    public static final String USER_EMAIL_FOUND =
            "Ya se encuentra un usuario registrado con el correo electronico.";
    public static final String API_KEY_AND_API_LOGIN_INVALID =
            "Apikey & ApiLogin Invalido o perfil ya verificado";
    public static final String PARLOR_NOT_FOUND = "Parlor not found";
}
