
package com.skall.paysafe.student.constants;

import lombok.Getter;

@Getter
public enum StatusCode {
    MERCHANT_DEBIT_CARD_VALIDATIONS_PENDING(2,
            "El comercio no tienes cuentas activas pero si pendientes por validacion"),
    MERCHANT_DOCUMENT_VALIDATIONS_PENDING(1,
            "Los documento correspondientes a este comercio se encuentran pendiente por validacion"),

    OK(0, "ok"),

    INTERNAL_SERVER_ERROR(-1, "internal server error"),
    BAD_REQUEST(-1, "bad request"),
    METHOD_NOT_ALLOWED(-1, "method not allowed"),
    UNSUPPORTED_MEDIA_TYPE(-1, "unsupported media type"),
    NOT_ACCEPTABLE(-1, "not acceptable"),
    NOT_FOUND(-1, "not found"),
    SERVICE_UNAVAILABLE(-1, "service unavailable"),
    ACCEPTED(-1, "accepted"),
    INVALID_DOCUMENT_NUMBER(-16, "Numero de documento invalido");

    private Integer code;
    private String description;

    private StatusCode(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}
